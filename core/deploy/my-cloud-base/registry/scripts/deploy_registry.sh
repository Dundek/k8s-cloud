#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

echo "==> Please enter the MyCloud API IP and port address (IP:PORT):"
read API_IP

cp ./nginx/nginx.conf.template ./nginx/nginx.conf
sed -i "s/<MYCLOUD_API_HOST_PORT>/$API_IP/g" /nginx/nginx.conf

vagrant up
if [ $? -eq 0 ]; then
    sleep 5
    REGISTRY_IP=$(vagrant ssh -c "hostname -I | cut -d' ' -f2" 2>/dev/null)
    echo "$REGISTRY_IP mycloud.registry.com registry.mycloud.org" >> /etc/hosts

    # touch ./configPrivateRegistry.sh
    # chmod a+rwx ./configPrivateRegistry.sh
    # vagrant ssh -c "cat /home/vagrant/configPrivateRegistry.sh" 2>/dev/null >> ./configPrivateRegistry.sh
    # dos2unix ./configPrivateRegistry.sh
    # sudo ./configPrivateRegistry.sh
    # rm -rf ./configPrivateRegistry.sh
    
    echo "[DONE] Deployed 'docker-registry' successfully!"
else
    vagrant halt
    vagrant destroy --force
    echo "[ERROR] Could not deploy the docker-registry"
fi

cd "$_PWD"