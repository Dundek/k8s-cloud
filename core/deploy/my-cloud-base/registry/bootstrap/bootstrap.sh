#!/bin/bash

# Update environment file
cat >>/etc/environment<<EOF
LANG=en_US.utf-8
LC_ALL=en_US.utf-8
EOF

# Install docker from Docker-ce repository
echo "[TASK 1] Create new partition"
echo -e "o\nn\np\n1\n\n\nw" | fdisk /dev/sdb

echo "[TASK 2] Mount partition"
mkfs.xfs -i size=512 /dev/sdb1
mkdir -p /mnt/docker-registry/data
echo '/dev/sdb1 /mnt/docker-registry/data xfs defaults 1 2' >> /etc/fstab
mount -a && mount

# Install docker from Docker-ce repository
echo "[TASK 3] Install docker container engine"
yum install -y -q yum-utils device-mapper-persistent-data lvm2 
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo 
yum install -y -q docker-ce >/dev/null 

# Enable docker service
echo "[TASK 4] Enable and start docker service"
systemctl enable docker >/dev/null 
systemctl start docker

# Disable SELinux
echo "[TASK 5] Disable SELinux"
setenforce 0
sed -i --follow-symlinks 's/^SELINUX=enforcing/SELINUX=disabled/' /etc/sysconfig/selinux

# Stop and disable firewalld
echo "[TASK 6] Stop and Disable firewalld"
systemctl disable firewalld >/dev/null 
systemctl stop firewalld

echo "[TASK 7] Install registry & certificates"
# Create auth
mkdir -p /opt/docker/containers/docker-registry/auth
mkdir -p /opt/docker/containers/nginx-registry/auth
docker run --entrypoint htpasswd registry -Bbn mycloud_master_user mycloud_master_pass > /opt/docker/containers/docker-registry/auth/htpasswd
docker run --entrypoint htpasswd registry -bn mycloud_master_user mycloud_master_pass > /opt/docker/containers/nginx-registry/auth/htpasswd

# Create self signed certificate
mkdir -p /opt/docker/containers/docker-registry/certs
printf "FR\nGaronne\nToulouse\nmycloud\nITLAB\nmycloud.registry.com\nmycloud@mycloud.com\n" | openssl req -newkey rsa:2048 -nodes -sha256 -x509 -days 365 -keyout /opt/docker/containers/docker-registry/certs/docker-registry.key -out /opt/docker/containers/docker-registry/certs/docker-registry.crt
printf "FR\nGaronne\nToulouse\nmycloud\nITLAB\nregistry.mycloud.org\nmycloud@mycloud.com\n" | openssl req -newkey rsa:2048 -nodes -sha256 -x509 -days 365 -keyout /opt/docker/containers/docker-registry/certs/nginx-registry.key -out /opt/docker/containers/docker-registry/certs/nginx-registry.crt

# Pull & run image
docker pull registry
docker pull nginx
docker run -d --name docker-registry --restart=always -p 5000:5000 -v /mnt/docker-registry/data/:/var/lib/registry -v /opt/docker/containers/docker-registry/auth:/auth -e "REGISTRY_AUTH=htpasswd" -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd -e REGISTRY_STORAGE_DELETE_ENABLED=true -v /opt/docker/containers/docker-registry/certs:/certs -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/docker-registry.crt -e REGISTRY_HTTP_TLS_KEY=/certs/docker-registry.key registry
docker run -d --name registry-nginx-container --restart=always -p 5043:443 -v /home/vagrant/nginx/nginx.conf:/etc/nginx/nginx.conf:ro -v /opt/docker/containers/docker-registry/certs:/certs -v /opt/docker/containers/nginx-registry/auth:/auth nginx

# Enable ssh password authentication
echo "[TASK 8] Enable ssh password authentication"
sed -i 's/^PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
systemctl reload sshd

# Set Root password
echo "[TASK 9] Set root password"
echo "kubeadmin" | passwd --stdin vagrant

# Give some extra time to make sure the VM got it's IP address
sleep 20

echo "[TASK 10] Generate client registry setup script"
M_IP="$(hostname -I | cut -d' ' -f2)"
CRT="$(cat /opt/docker/containers/docker-registry/certs/docker-registry.crt)"
CRT_NGINX="$(cat /opt/docker/containers/docker-registry/certs/nginx-registry.crt)"

echo "#!/bin/bash"  >> /home/vagrant/configPrivateRegistry.sh
# echo "echo \"$M_IP mycloud.registry.com registry.mycloud.org docker-registry\"  >> /etc/hosts" >> /home/vagrant/configPrivateRegistry.sh

echo "mkdir -p /etc/docker/certs.d/mycloud.registry.com:5000" >> /home/vagrant/configPrivateRegistry.sh
echo "cat <<EOT >> /etc/docker/certs.d/mycloud.registry.com:5000/ca.crt" >> /home/vagrant/configPrivateRegistry.sh
echo "$CRT"  >> /home/vagrant/configPrivateRegistry.sh
echo "EOT"  >> /home/vagrant/configPrivateRegistry.sh

echo "mkdir -p /etc/docker/certs.d/registry.mycloud.org:5043" >> /home/vagrant/configPrivateRegistry.sh
echo "cat <<EOT >> /etc/docker/certs.d/registry.mycloud.org:5043/ca.crt" >> /home/vagrant/configPrivateRegistry.sh
echo "$CRT_NGINX"  >> /home/vagrant/configPrivateRegistry.sh
echo "EOT"  >> /home/vagrant/configPrivateRegistry.sh

echo "systemctl stop docker && systemctl start docker"  >> /home/vagrant/configPrivateRegistry.sh
#echo "printf \"mycloud_master_pass\" | docker login registry.mycloud.org:5043 --username mycloud_master_user --password-stdin"  >> /home/vagrant/configPrivateRegistry.sh

chmod +x /home/vagrant/configPrivateRegistry.sh

echo ""
echo "------------------- RUN ON CLIENT --------------------"
cat  /home/vagrant/configPrivateRegistry.sh
echo "------------------------------------------------------"
echo ""
echo "Once the script executed, you can login to the private repository:"
echo ""
echo "\$ docker login registry.mycloud.org:5043"
echo "NOTE: Username: mycloud_master_user, Password: mycloud_master_pass"
echo ""
echo "To push an image to the new registry:"
echo ""
echo "\$ docker tag <image name>:<image tag> registry.mycloud.org:5043/<image name>:<image tag>"
echo "\$ docker push registry.mycloud.org:5043/<image name>:<image tag>"
echo ""
echo "[INFO] For K8S, execute the scripe as sudo on each node. You will have to create a secret to hold the basic auth credentials in order to pull images:"
echo "https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/#before-you-begin"