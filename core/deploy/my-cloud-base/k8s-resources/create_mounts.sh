#!/bin/bash

if [ -d "/mnt/postgres-pv" ]; then
    echo "PostgreSQL mounts found, moving on..."
else
    sudo mkdir -p /mnt/postgres-pv
    sudo chown $(id -u):$(id -g) /mnt/postgres-pv
    sudo chmod a+rw /mnt/postgres-pv
fi

if [ -d "/mnt/mosquitto-pv" ]; then
    echo "Mosquitto mounts found, moving on..."
else
    sudo mkdir -p /mnt/mosquitto-pv/config
    sudo mkdir -p /mnt/mosquitto-pv/data
    sudo mkdir -p /mnt/mosquitto-pv/log
    sudo chown $(id -u):$(id -g) /mnt/mosquitto-pv/config/
    sudo chmod a+rw /mnt/mosquitto-pv/data/
    sudo chmod a+rw /mnt/mosquitto-pv/log/
fi