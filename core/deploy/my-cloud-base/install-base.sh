#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR

install_core_components() {
    echo "[STEP 8] Installing core components ..."

    cd ../my-cloud-base # Position cmd in mycloud base folder
    MASTER_IP=$(vagrant ssh -c "hostname -I | cut -d' ' -f2" 2>/dev/null)
    cd src # Position cmd in src folder
    
    cp env-template env

    #MOSQUITTO_PORT=$(vagrant ssh -c "kubectl describe service mosquitto-service | grep 'NodePort:' | grep 'tcp-port' | tr -s \" \" | cut -d ' ' -f 3 | cut -d '/' -f 1" 2>/dev/null)
    #POSTGRES_PORT=$(vagrant ssh -c "kubectl describe service postgres-service | grep 'NodePort:' | tr -s \" \" | cut -d ' ' -f 3 | cut -d '/' -f 1" 2>/dev/null)
    
    if [ "$PW" == "" ]; then
        echo "==> Please enter the PostgreSQL database password:"
        read PW  
    fi

    echo "==> Specify a system admin user name for the API server:"
    read SYSADMIN_USER 

    echo "==> Specify a system admin password for that user:"
    read SYSADMIN_PASS 

    VM_BASE=$HOME/my-cloud/vm_base
    APP_TMP=$HOME/my-cloud/tmp

    sed -i "s/<MASTER_IP>/$MASTER_IP/g" ./env
    sed -i "s/<REGISTRY_IP>/$REGISTRY_IP/g" ./env
    sed -i "s/<DB_PORT>/$POSTGRES_PORT/g" ./env
    sed -i "s/<DB_PASS>/$PW/g" ./env
    sed -i "s/<MOSQUITTO_PORT>/$MOSQUITTO_PORT/g" ./env
    sed -i "s/<SYSADMIN_USER>/$SYSADMIN_USER/g" ./env
    sed -i "s/<SYSADMIN_PASS>/$SYSADMIN_PASS/g" ./env
    sed -i "s/<VM_BASE_HOME>/${VM_BASE//\//\\/}/g" ./env
    sed -i "s/<APP_TMP_DIR>/${APP_TMP//\//\\/}/g" ./env
    sed -i "s/<NET_INTEFACE>/$INET/g" ./env

    cp env host-node/.env
    cp env task-controller/.env
    cp env api/.env

    rm env

    # Start MyCloud proxy server
    mkdir -p $HOME/.mycloud/nginx/conf.d
    mkdir -p $HOME/.mycloud/nginx/letsencrypt
    cp nginx_resources/default.conf $HOME/.mycloud/nginx/conf.d
    cp nginx_resources/nginx.conf $HOME/.mycloud/nginx
    docker run -d --name mycloud-nginx --restart unless-stopped --network host -v $HOME/.mycloud/nginx/conf.d:/etc/nginx/conf.d -v $HOME/.mycloud/nginx/nginx.conf:/etc/nginx/nginx.conf -v $HOME/.mycloud/nginx/letsencrypt:/etc/letsencrypt nginx:1.17.9-alpine
    
    API_DEPLOYED=$(pm2 ls | grep "mycloud-api")
    if [ "$API_DEPLOYED" == "" ]; then
        cd api
        npm i
        pm2 start src/ --watch --name mycloud-api --time --log ../_logs/api.logs
        cd ..
    else
        pm2 restart mycloud-api
    fi
    # HOST_NODE_DEPLOYED=$(pm2 ls | grep "mycloud-host-node")
    # if [ "$HOST_NODE_DEPLOYED" == "" ]; then
    #     cd host-node
    #     npm i
    #     pm2 start index.js --watch --name mycloud-host-node --time --log ../_logs/host-node.logs
    #     cd ..
    # else
    #     pm2 restart mycloud-host-node
    # fi
    TASK_CONTROLLER_DEPLOYED=$(pm2 ls | grep "mycloud-task-controller")
    if [ "$TASK_CONTROLLER_DEPLOYED" == "" ]; then
        cd task-controller
        npm i
        pm2 start index.js --watch --name mycloud-task-controller --time --log ../_logs/task-controller.logs
        cd ..
    else
        pm2 restart mycloud-task-controller
    fi

    cd .. # Back to repo root folder
}

pull_git() {
    echo "[STEP 2] Pulling repo from GIT..."
    if [ ! -d "$HOME/my-cloud" ] 
    then
        mkdir $HOME/my-cloud
        git clone https://gitlab.com/Dundek/k8s-cloud.git $HOME/my-cloud
    fi
    
}

install_postgress() {
    echo "[STEP 5] Installing PostgreSQL secret ..."
    HAS_POSTGRES_SECRET=$(vagrant ssh -c "kubectl get secrets | grep 'postgres-secrets'" 2>/dev/null)
    if [ "$HAS_POSTGRES_SECRET" == "" ]; then
        echo ""
        echo "==> Please enter your PostgreSQL database password:"
        read PW
        PW_B64=$(echo -n "$PW" | base64)

        vagrant ssh -c "cp \$HOME/k8s-resources/postgres/secret-template.yml \$HOME/k8s-resources/postgres/secret.yml" 2>/dev/null
        vagrant ssh -c "sed -i \"s/ROOT_PASSWORD:/ROOT_PASSWORD: $PW_B64/g\" \$HOME/k8s-resources/postgres/secret.yml" 2>/dev/null
        vagrant ssh -c "kubectl apply -f \$HOME/k8s-resources/postgres/secret.yml" 2>/dev/null    
    else
        echo "[INFO] PostgreSQL secret already installed: $HAS_POSTGRES_SECRET"    
    fi

    echo "[STEP 6] Installing PostgreSQL ..."
    HAS_POSTGRES_SERVICE=$(vagrant ssh -c "kubectl get services | grep 'postgres-service'" 2>/dev/null)
    if [ "$HAS_POSTGRES_SERVICE" == "" ]; then
        vagrant ssh -c "kubectl apply -f \$HOME/k8s-resources/postgres/deployment.yml" 2>/dev/null
        sleep 5
    else
        echo "[INFO] PostgreSQL already installed"    
    fi

    POSTGRES_PORT=$(vagrant ssh -c "kubectl get services | grep 'postgres-service' | cut -d: -f2 | cut -d/ -f1")
}

install_mosquitto() {
    echo "[STEP 7] Installing Mosquitto ..."
    HAS_MOSQUITTO_CONF=$(vagrant ssh -c "ls /mnt/mosquitto-pv/config | grep 'mosquitto.conf'" 2>/dev/null)
    if [ "$HAS_MOSQUITTO_CONF" == "" ]; then
         vagrant ssh -c "cp \$HOME/k8s-resources/mosquitto/config/mosquitto.conf /mnt/mosquitto-pv/config" 2>/dev/null
    fi
    HAS_MOSQUITTO_SERVICE=$(vagrant ssh -c "kubectl get services | grep 'mosquitto-service'" 2>/dev/null)
    if [ "$HAS_MOSQUITTO_SERVICE" == "" ]; then
        vagrant ssh -c "kubectl apply -f \$HOME/k8s-resources/mosquitto/deployment.yml" 2>/dev/null   
        sleep 5 
    else
        echo "[INFO] Mosquitto already installed"    
    fi
    MOSQUITTO_PORT=$(vagrant ssh -c "kubectl get services | grep 'mosquitto-service' | cut -d: -f2 | cut -d/ -f1")
}

dependencies () {
    echo "[STEP 1] Installing dependencies..."
    #sudo apt update
    VAGRANT_EXISTS=$(command -v vagrant)
    VIRTUALBOX_EXISTS=$(command -v vboxmanage)
    DOS2UNIX_EXISTS=$(command -v dos2unix)
    if [ "$VIRTUALBOX_EXISTS" == "" ]; then
        # if [[ $(uname -s) == Darwin ]]; then
            
        # fi
        # if [[ $(uname -s) == Linux ]]; then
            
        # fi
        apt install virtualbox -y &> /dev/null
    fi
    if [ "$VAGRANT_EXISTS" == "" ]; then
        apt install vagrant -y &> /dev/null
    fi
    if [ "$DOS2UNIX_EXISTS" == "" ]; then
        apt install dos2unix -y &> /dev/null
    fi

    NODE_EXISTS=$(command -v node)
    if [ "$NODE_EXISTS" == "" ]; then
        curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh &> /dev/null
        sudo bash nodesource_setup.sh &> /dev/null
        sudo apt install nodejs -y &> /dev/null
        rm -rf nodesource_setup.sh &> /dev/null
    fi

    PM2_EXISTS=$(command -v pm2)
    if [ "$PM2_EXISTS" == "" ]; then
        sudo npm install pm2@latest -g &> /dev/null
        sudo chown $(id -u):$(id -g) $HOME/.pm2/rpc.sock $HOME/.pm2/pub.sock
        sudo pm2 install pm2-logrotate
        pm2 set pm2-logrotate:max_size 10M
        pm2 set pm2-logrotate:compress true
        pm2 set pm2-logrotate:rotateInterval '* * 1 * *'
    fi

    GIT_EXISTS=$(command -v git)
    if [ "$GIT_EXISTS" == "" ]; then
        sudo apt install git -y &> /dev/null
    fi
}

cluster_deploy () {
    VM_EXISTS=$(vboxmanage list vms | grep "kmaster.mycloud-base")
    
    if [ "$VM_EXISTS" == "" ]; then
        echo "[STEP 4] Deploying the MyCloud cluster... (this might take up to 10 minutes on slower networks / machines)"
        vagrant up &> /dev/null
        if [ $? -eq 0 ]; then
            sleep 5
            vagrant ssh -c "kubectl taint nodes --all node-role.kubernetes.io/master-" &> /dev/null
            sleep 10
        else
            vagrant halt &> /dev/null
            vagrant destroy --force &> /dev/null
            echo "[ERROR] Could not deploy the mycloud base components"
            exit 1
        fi
    else
        VM_STATE=$(vboxmanage showvminfo "kmaster.mycloud-base" | grep -e ^State | sed s/\ \ //g | cut -d: -f2- | cut -d ' ' -f 2)
        if [ "$VM_STATE" == "running" ]; then
            echo "[INFO] VM is already up and running"
        elif [ "$VM_STATE" == "paused" ]; then
            vboxmanage controlvm kmaster.mycloud-base resume
            sleep 3
        elif [ "$VM_STATE" == "powered" ]; then
            vboxmanage startvm kmaster.mycloud-base --type headless
            sleep 10
        elif [ "$VM_STATE" == "saved" ]; then
            vboxmanage startvm kmaster.mycloud-base --type headless
            sleep 10
        else
            echo "[ERROR] Unknown VM state: $VM_STATE"
            exit 1
        fi
    fi
}

registry_deploy () {
    VM_EXISTS=$(vboxmanage list vms | grep "registry.mycloud")
    
    if [ "$VM_EXISTS" == "" ]; then
        echo "[STEP 4 bis] Deploying the MyCloud registry... (this might take up to 10 minutes on slower networks / machines)"
        cd registry

        cp ./nginx/nginx.conf.template ./nginx/nginx.conf
        sed -i "s/<MYCLOUD_API_HOST_PORT>/$LOCAL_IP/g" ./nginx/nginx.conf

        vagrant up &> /dev/null
        if [ $? -eq 0 ]; then
            sleep 5
            REGISTRY_IP=$(vagrant ssh -c "hostname -I | cut -d' ' -f2" 2>/dev/null)
            echo "$REGISTRY_IP mycloud.registry.com registry.mycloud.org" >> /etc/hosts
            cd ..
        else
            vagrant halt &> /dev/null
            vagrant destroy --force &> /dev/null
            echo "[ERROR] Could not deploy the mycloud registry"
            exit 1
        fi
    else
        REGISTRY_IP=$(vagrant ssh -c "hostname -I | cut -d' ' -f2" 2>/dev/null)

        VM_STATE=$(vboxmanage showvminfo "registry.mycloud" | grep -e ^State | sed s/\ \ //g | cut -d: -f2- | cut -d ' ' -f 2)
        if [ "$VM_STATE" == "running" ]; then
            echo "[INFO] VM is already up and running"
        elif [ "$VM_STATE" == "paused" ]; then
            vboxmanage controlvm registry.mycloud resume
            sleep 3
        elif [ "$VM_STATE" == "powered" ]; then
            vboxmanage startvm registry.mycloud --type headless
            sleep 10
        elif [ "$VM_STATE" == "saved" ]; then
            vboxmanage startvm registry.mycloud --type headless
            sleep 10
        else
            echo "[ERROR] Unknown VM state: $VM_STATE"
            exit 1
        fi
    fi
}

if [[ $(uname -s) == Darwin ]]; then
    INET=$(route get 10.10.10.10 | grep 'interface' | tr -s " " | sed -e 's/^[ \t]*//' | cut -d ' ' -f 2)
fi
if [[ $(uname -s) == Linux ]]; then
    INET=$(route | grep '^default' | grep -o '[^ ]*$')
fi
LOCAL_IP=ip -4 addr show $INET | grep -oP '(?<=inet\s)\d+(\.\d+){3}'

# Install dependencies
dependencies
# Clone repo
pull_git
# Start K8S Cluster for MyCloud
cluster_deploy
# Start Registry VM for MyCloud
registry_deploy

# Prepare master VM for service deployments
echo "[STEP 3] Preparing master environement..."
vagrant ssh -c "\$HOME/k8s-resources/create_mounts.sh" 2>/dev/null
HAS_STORAGE_CLASS=$(vagrant ssh -c "kubectl get storageClass | grep 'local-storage'" 2>/dev/null)
if [ "$HAS_STORAGE_CLASS" == "" ]; then
    vagrant ssh -c "kubectl apply -f \$HOME/k8s-resources/postgres/storageClass.yml" 2>/dev/null
fi
# Install DB (PostgreSQL)
install_postgress
# Install MQTT (Mosquitto)
install_mosquitto
# Install the core components
install_core_components

echo "[DONE] MyCloud deployed successfully!"
echo ""
echo "When installing the host-nodes, use the following details:"
echo "  The master IP is $MASTER_IP"
echo "  The Postgress PORT is $POSTGRES_PORT"
echo "  The Mosquitto PORT is $MOSQUITTO_PORT"

cd "$_PWD"