#!/bin/bash

rm -rf mycloud-master.box
vagrant box remove mycloud-master

VBV=$(vboxmanage --version | cut -d '_' -f1)
sed -i "s/<VBV>/$VBV/g" ./bootstrap.sh

vagrant plugin install vagrant-vbguest

vagrant up
# vagrant reload --provision
vagrant halt

vagrant package --output mycloud-master.box
vagrant box add mycloud-master mycloud-master.box

vagrant destroy -f
rm -rf .vagrant
sed -i "s/$VBV/<VBV>/g" ./bootstrap.sh