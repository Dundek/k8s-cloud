#!/bin/bash

rm -rf mycloud-worker.box
vagrant box remove mycloud-worker

VBV=$(vboxmanage --version | cut -d '_' -f1)
sed -i "s/<VBV>/$VBV/g" ./bootstrap.sh

vagrant plugin install vagrant-vbguest

vagrant up
# vagrant reload --provision
vagrant halt

vagrant package --output mycloud-worker.box
vagrant box add mycloud-worker mycloud-worker.box

vagrant destroy -f
rm -rf .vagrant
sed -i "s/$VBV/<VBV>/g" ./bootstrap.sh