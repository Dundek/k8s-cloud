#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

cp -r ../.vagrantstates/deploy_docker_registry ./ && mv ./deploy_docker_registry ./.vagrant

touch ./configPrivateRegistry.sh
chmod a+rwx ./configPrivateRegistry.sh
vagrant ssh -c "cat /home/vagrant/configPrivateRegistry.sh" 2>/dev/null >> ./configPrivateRegistry.sh
dos2unix ./configPrivateRegistry.sh
sudo ./configPrivateRegistry.sh
rm -rf ./configPrivateRegistry.sh

rm -rf ./.vagrant

cd "$_PWD"