#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

vagrant up

if [ $? -eq 0 ]; then
    touch ./configPrivateRegistry.sh
    chmod a+rwx ./configPrivateRegistry.sh
    vagrant ssh -c "cat /home/vagrant/configPrivateRegistry.sh" 2>/dev/null >> ./configPrivateRegistry.sh
    dos2unix ./configPrivateRegistry.sh
    sudo ./configPrivateRegistry.sh
    rm -rf ./configPrivateRegistry.sh

    rm -rf ../.vagrantstates/deploy_docker_registry
    cp -r .vagrant ../.vagrantstates/deploy_docker_registry
    echo "[DONE] Deployed 'docker-registry' successfully!"
else
    vagrant halt
    vagrant destroy --force
    echo "[ERROR] Could not deploy the docker-registry"
fi

rm -rf ./.vagrant

cd "$_PWD"