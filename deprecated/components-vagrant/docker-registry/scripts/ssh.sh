#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export HOSTNAME="$(hostname)"

cp -r ../.vagrantstates/deploy_docker_registry ./ && mv ./deploy_docker_registry ./.vagrant

vagrant ssh

rm -rf ./.vagrant

cd "$_PWD"