#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export CLOUD_ORG=$1

cp deploy_master_Vagrantfile Vagrantfile
cp -r ../.vagrantstates/$CLOUD_ORG-deploy-master ./ && mv ./$CLOUD_ORG-deploy-master ./.vagrant

echo "=> CLUSTER NODES:"
echo ""
vagrant ssh -c "kubectl get nodes" kmaster.$CLOUD_ORG 2>/dev/null
echo ""
echo ""
echo "=> CLUSTER NODES IPs:"
echo ""
vagrant ssh -c "kubectl describe nodes | grep 'IPv4Address\|Name:'" kmaster.$CLOUD_ORG 2>/dev/null
echo ""
echo ""
echo "=> CLUSTER PODS:"
echo ""
vagrant ssh -c "kubectl get pods --all-namespaces" kmaster.$CLOUD_ORG 2>/dev/null

rm -rf ./Vagrantfile
rm -rf ./.vagrant

cd "$_PWD"