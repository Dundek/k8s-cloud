#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export CLOUD_ORG=$1
export CLOUD_WIND=$2

cp deploy_worker_Vagrantfile ./Vagrantfile
cp -r ../.vagrantstates/$CLOUD_ORG-deploy-worker-$CLOUD_WIND ./ && mv ./$CLOUD_ORG-deploy-worker-$CLOUD_WIND ./.vagrant

vagrant halt
vagrant up

rm -rf ../.vagrantstates/$CLOUD_ORG-deploy-worker-$CLOUD_WIND
cp -r .vagrant ../.vagrantstates/$CLOUD_ORG-deploy-worker-$CLOUD_WIND

rm -rf ./Vagrantfile
rm -rf ./.vagrant

cd "$_PWD"