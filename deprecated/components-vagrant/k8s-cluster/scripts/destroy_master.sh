#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export CLOUD_ORG=$1

echo "[TASK 1] Deprovision worker VM"
cp deploy_master_Vagrantfile Vagrantfile
cp -r ../.vagrantstates/$CLOUD_ORG-deploy-master ./ && mv ./$CLOUD_ORG-deploy-master ./.vagrant

vagrant halt
vagrant destroy --force

rm -rf ./Vagrantfile
rm -rf ./.vagrant
rm -rf ../.vagrantstates/$CLOUD_ORG-deploy-master

cd "$_PWD"

echo "[DONE]"