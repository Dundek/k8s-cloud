#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

if [ "$1" == "--help" ]; then
   echo "USAGE: ./gluster_uninstall"
else
    export HOSTNAME="$(hostname)"

    cp ../gluster/Vagrantfile ./
    cp -r ../.vagrantstates/deploy-gluster-$HOSTNAME ./ && mv ./deploy-gluster-$HOSTNAME ./.vagrant

    vagrant ssh -c "sudo yum remove glusterfs-server -y" gluster-$HOSTNAME
    
    rm -rf ./Vagrantfile
    rm -rf ./.vagrant

    echo "[DONE]"
fi

cd "$_PWD"