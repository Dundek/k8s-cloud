#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export CLOUD_ORG=$1

cp deploy_master_Vagrantfile Vagrantfile
cp -r ../.vagrantstates/$CLOUD_ORG-deploy-master ./ && mv ./$CLOUD_ORG-deploy-master ./.vagrant

CFG_CERT=$(vagrant ssh -c "sudo cat /etc/kubernetes/admin.conf" 2>/dev/null kmaster.$CLOUD_ORG)

mkdir -p $HOME/.kube/
rm -rf $HOME/.kube/admin.conf
echo "$CFG_CERT" >> $HOME/.kube/admin.conf
chown $(id -u):$(id -g) $HOME/.kube/admin.conf
echo "export KUBECONFIG=$HOME/.kube/admin.conf" | tee -a ~/.bashrc
source ~/.bashrc

rm -rf ./Vagrantfile
rm -rf ./.vagrant

cd "$_PWD"
