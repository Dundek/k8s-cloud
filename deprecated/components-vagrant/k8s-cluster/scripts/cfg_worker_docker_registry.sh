#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export CLOUD_ORG=$1
export CLOUD_WIND=$2
export REGISTRY_IP=$3

cp deploy_worker_Vagrantfile ./Vagrantfile 
cp -r ../.vagrantstates/$CLOUD_ORG-deploy-worker-$CLOUD_WIND ./ && mv ./$CLOUD_ORG-deploy-worker-$CLOUD_WIND ./.vagrant

vagrant ssh -c "sshpass -p 'kubeadmin' sudo scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no vagrant@$REGISTRY_IP:/home/vagrant/configPrivateRegistry.sh /home/vagrant/configPrivateRegistry.sh" kworker.$CLOUD_ORG.$CLOUD_WIND
vagrant ssh -c "sudo /home/vagrant/configPrivateRegistry.sh" kmaster.$CLOUD_ORG

rm -rf ./Vagrantfile
rm -rf ./.vagrant

cd "$_PWD"