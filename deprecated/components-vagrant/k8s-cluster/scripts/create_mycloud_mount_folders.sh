#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export CLOUD_ORG=$1
export CLOUD_WIND=$2

cp deploy_worker_Vagrantfile ./Vagrantfile 
cp -r ../.vagrantstates/$CLOUD_ORG-deploy-worker-$CLOUD_WIND ./ && mv ./$CLOUD_ORG-deploy-worker-$CLOUD_WIND ./.vagrant




rm -rf ./Vagrantfile
rm -rf ./.vagrant

cp deploy_master_Vagrantfile Vagrantfile
cp -r ../.vagrantstates/$CLOUD_ORG-deploy-master ./ && mv ./$CLOUD_ORG-deploy-master ./.vagrant

vagrant ssh -c "sudo mkdir -p /mnt/postgres-pv" 2>/dev/null kmaster.$CLOUD_ORG
vagrant ssh -c "sudo chown vagrant:vagrant /mnt/postgres-pv" 2>/dev/null kmaster.$CLOUD_ORG
vagrant ssh -c "sudo chmod a+rw /mnt/postgres-pv" 2>/dev/null kmaster.$CLOUD_ORG

vagrant ssh -c "sudo mkdir -p /mnt/mosquitto-pv/config" 2>/dev/null kmaster.$CLOUD_ORG
vagrant ssh -c "sudo mkdir -p /mnt/mosquitto-pv/data" 2>/dev/null kmaster.$CLOUD_ORG
vagrant ssh -c "sudo mkdir -p /mnt/mosquitto-pv/log" 2>/dev/null kmaster.$CLOUD_ORG
vagrant ssh -c "sudo chown vagrant:vagrant /mnt/mosquitto-pv/config/" 2>/dev/null kmaster.$CLOUD_ORG
vagrant ssh -c "sudo chmod a+rw /mnt/mosquitto-pv/data/" 2>/dev/null kmaster.$CLOUD_ORG
vagrant ssh -c "sudo chmod a+rw /mnt/mosquitto-pv/log/" 2>/dev/null kmaster.$CLOUD_ORG

rm -rf ./Vagrantfile
rm -rf ./.vagrant

cd "$_PWD"