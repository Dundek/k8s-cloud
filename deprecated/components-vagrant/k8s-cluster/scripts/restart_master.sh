#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export CLOUD_ORG=$1

cp deploy_master_Vagrantfile Vagrantfile
cp -r ../.vagrantstates/$CLOUD_ORG-deploy-master ./ && mv ./$CLOUD_ORG-deploy-master ./.vagrant

vagrant halt
vagrant up

rm -rf ../.vagrantstates/$CLOUD_ORG-deploy-master
cp -r .vagrant ../.vagrantstates/$CLOUD_ORG-deploy-master

rm -rf ./Vagrantfile
rm -rf ./.vagrant

cd "$_PWD"