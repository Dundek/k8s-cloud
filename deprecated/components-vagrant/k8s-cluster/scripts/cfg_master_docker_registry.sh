#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export CLOUD_ORG=$1
export REGISTRY_IP=$2

cp deploy_master_Vagrantfile Vagrantfile
cp -r ../.vagrantstates/$CLOUD_ORG-deploy-master ./ && mv ./$CLOUD_ORG-deploy-master ./.vagrant

vagrant ssh -c "sshpass -p 'kubeadmin' sudo scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no vagrant@$REGISTRY_IP:/home/vagrant/configPrivateRegistry.sh /home/vagrant/configPrivateRegistry.sh" kmaster.$CLOUD_ORG
vagrant ssh -c "sudo /home/vagrant/configPrivateRegistry.sh" kmaster.$CLOUD_ORG
vagrant ssh -c "kubectl create secret docker-registry mdcloud.registry.com --docker-server=mdcloud.registry.com:5000 --docker-username=registry_user --docker-password=registry_pass" kmaster.$CLOUD_ORG

rm -rf Vagrantfile
rm -rf ./.vagrant

cd "$_PWD"