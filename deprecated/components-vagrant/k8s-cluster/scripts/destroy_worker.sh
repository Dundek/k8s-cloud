#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export CLOUD_ORG=$1
export CLOUD_WIND=$2

if [ "$3" == "--no-master-drain" ]; then
    cp deploy_master_Vagrantfile Vagrantfile
    cp -r ../.vagrantstates/$CLOUD_ORG-deploy-master ./ && mv ./$CLOUD_ORG-deploy-master ./.vagrant

    echo "[TASK 1] Drain & delete worker from cluster"
    vagrant ssh -c "kubectl drain kworker.$CLOUD_ORG.$CLOUD_WIND --ignore-daemonsets --delete-local-data" 2>/dev/null kmaster.$CLOUD_ORG
    vagrant ssh -c "kubectl delete node kworker.$CLOUD_ORG.$CLOUD_WIND" 2>/dev/null kmaster.$CLOUD_ORG

    rm -rf ./Vagrantfile
    rm -rf ./.vagrant
fi

echo "[TASK 2] Deprovision worker VM"
cp deploy_worker_Vagrantfile ./Vagrantfile
cp -r ../.vagrantstates/$CLOUD_ORG-deploy-worker-$CLOUD_WIND ./ && mv ./$CLOUD_ORG-deploy-worker-$CLOUD_WIND ./.vagrant

vagrant halt
vagrant destroy --force

rm -rf ./Vagrantfile
rm -rf ./.vagrant
rm -rf ../.vagrantstates/$CLOUD_ORG-deploy-worker-$CLOUD_WIND

cd "$_PWD"

echo "[DONE]"