#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export CLOUD_ORG=$1
export CLOUD_WIND=$2

if [ -z "$3" ]
then
    echo "[TASK 0] Get cluster IP $M_IP"
    cp deploy_master_Vagrantfile Vagrantfile
    cp -r ../.vagrantstates/$CLOUD_ORG-deploy-master ./ && mv ./$CLOUD_ORG-deploy-master ./.vagrant
    M_IP="$(vagrant ssh -c "hostname -I | cut -d' ' -f2" 2>/dev/null kmaster.$CLOUD_ORG)"
    echo "Cluster master IP: $M_IP"
    vagrant ssh -c "sudo ~/gentoken.sh" kmaster.$CLOUD_ORG
    rm -rf ./Vagrantfile
    rm -rf ./.vagrant
else
    M_IP=$3
fi

cp ./deploy_worker_Vagrantfile ./Vagrantfile
vagrant up
if [ $? -eq 0 ]; then
    rm -rf ../.vagrantstates/$CLOUD_ORG-deploy-worker-$CLOUD_WIND
    cp -r .vagrant ../.vagrantstates/$CLOUD_ORG-deploy-worker-$CLOUD_WIND

    echo "[TASK W.3] Join worker to the cluster on IP $M_IP"
    vagrant ssh -c "sshpass -p 'kubeadmin' sudo scp -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no vagrant@$M_IP:/joincluster.sh /joincluster.sh" 2>/dev/null kworker.$CLOUD_ORG.$CLOUD_WIND
    vagrant ssh -c "sudo bash /joincluster.sh" 2>/dev/null kworker.$CLOUD_ORG.$CLOUD_WIND

    echo "[DONE] Deployed worker 'kworker.$CLOUD_ORG.$CLOUD_WIND' successfully!"
else
    vagrant halt
    vagrant destroy --force
    echo "[ERROR] Could not deploy the worker node"
fi
# rm -rf ./Vagrantfile
# rm -rf ./.vagrant

cd "$_PWD"