#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export CLOUD_ORG=$1

cp deploy_master_Vagrantfile Vagrantfile
cp -r ../.vagrantstates/$CLOUD_ORG-deploy-master ./ && mv ./$CLOUD_ORG-deploy-master ./.vagrant

vagrant ssh -c "hostname -I | cut -d' ' -f2" 2>/dev/null kmaster.$CLOUD_ORG

rm -rf ./Vagrantfile
rm -rf ./.vagrant

cd "$_PWD"