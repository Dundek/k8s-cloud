#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export CLOUD_ORG=$1

cp deploy_master_Vagrantfile Vagrantfile
cp -r ../.vagrantstates/$CLOUD_ORG-deploy-master ./ && mv ./$CLOUD_ORG-deploy-master ./.vagrant

echo "On the machine where you wish to use kubectl, create a file in your local \$HOME/.kube folder named 'admin.conf':"
echo ""
echo "\$ mkdir \$HOME/.kube"
echo "\$ touch \$HOME/.kube/admin.conf"
echo "\$ chown \$(id -u):\$(id -g) \$HOME/.kube/admin.conf"
echo "\$ echo "export KUBECONFIG=\$HOME/.kube/admin.conf" | tee -a ~/.bashrc"
echo ""
echo "Now paste the following content into the file \"\$HOME/.kube/admin.conf\":"
echo ""
echo "---------------------------------------------------------------------"
vagrant ssh -c "sudo cat /etc/kubernetes/admin.conf" kmaster.$CLOUD_ORG 2>/dev/null
echo "---------------------------------------------------------------------"
echo ""
echo "Source the ~/.bashrc file:"
echo ""
echo "\$ source ~/.bashrc"
echo ""
echo "Done!"

rm -rf ./Vagrantfile
rm -rf ./.vagrant

cd "$_PWD"