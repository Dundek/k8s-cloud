#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export CLOUD_ORG=$1

cp deploy_master_Vagrantfile Vagrantfile
vagrant up
if [ $? -eq 0 ]; then
    if [ "$2" == "--untaint" ]; then
        echo "[EXTRA] Make master node also a worker node"
        vagrant ssh -c "kubectl taint nodes --all node-role.kubernetes.io/master-" 2>/dev/null kmaster.$CLOUD_ORG
    fi

    rm -rf ../.vagrantstates/$CLOUD_ORG-deploy-master
    cp -r .vagrant ../.vagrantstates/$CLOUD_ORG-deploy-master
    echo "[DONE] Deployed master 'kmaster.$CLOUD_ORG' successfully!"
else
    vagrant halt
    vagrant destroy --force
    echo "[ERROR] Could not deploy the master node"
fi
rm -rf ./Vagrantfile
rm -rf ./.vagrant

cd "$_PWD"