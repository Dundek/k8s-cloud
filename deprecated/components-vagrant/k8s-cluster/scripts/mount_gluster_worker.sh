#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ..

export CLOUD_ORG=$1
export CLOUD_WIND=$2
export VOL_NAME=$3
export GLUSTER_IP=$4

cp deploy_worker_Vagrantfile ./Vagrantfile 
cp -r ../.vagrantstates/$CLOUD_ORG-deploy-worker-$CLOUD_WIND ./ && mv ./$CLOUD_ORG-deploy-worker-$CLOUD_WIND ./.vagrant

vagrant ssh -c "sudo yum install centos-release-gluster -y" 2>/dev/null kworker.$CLOUD_ORG.$CLOUD_WIND
vagrant ssh -c "sudo yum install glusterfs-server -y" 2>/dev/null kworker.$CLOUD_ORG.$CLOUD_WIND
vagrant ssh -c "sudo systemctl disable glusterd" 2>/dev/null kworker.$CLOUD_ORG.$CLOUD_WIND
vagrant ssh -c "sudo systemctl stop glusterd" 2>/dev/null kworker.$CLOUD_ORG.$CLOUD_WIND

vagrant ssh -c "sudo mkdir /mnt/$VOL_NAME" 2>/dev/null kworker.$CLOUD_ORG.$CLOUD_WIND
vagrant ssh -c "sudo mount.glusterfs $GLUSTER_IP:/$VOL_NAME /mnt/$VOL_NAME" 2>/dev/null kworker.$CLOUD_ORG.$CLOUD_WIND
vagrant ssh -c "echo '$GLUSTER_IP:/$VOL_NAME   /mnt/$VOL_NAME  glusterfs _netdev,auto,x-systemd.automount 0 0' | sudo tee -a /etc/fstab" 2>/dev/null kworker.$CLOUD_ORG.$CLOUD_WIND

rm -rf ./Vagrantfile
rm -rf ./.vagrant

cd "$_PWD"

