var mqtt = require('mqtt')

var options = {
    port: 30346,
    host: 'mqtt://192.168.68.161', //K8S master IP
    encoding: 'utf8'
};

var client  = mqtt.connect('mqtt://192.168.68.161', options)
 
client.on('connect', function () {
  client.subscribe('presence', function (err) {
    if (!err) {
      client.publish('presence', 'Hello mqtt')
    }
  })
})
 
client.on('message', function (topic, message) {
  // message is Buffer
  console.log(message.toString())
  client.end()
})