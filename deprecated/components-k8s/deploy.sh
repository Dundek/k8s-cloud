#!/bin/bash

_DIR="$(cd "$(dirname "$0")" && pwd)"
_PWD="$(pwd)"

cd $_DIR && cd ../../..

./core/deploy/components-vagrant/k8s-cluster/scripts/setup_kubectl_mycloud.sh mycloud
./core/deploy/components-vagrant/k8s-cluster/scripts/cfg_master_docker_registry.sh mycloud 192.168.68.160
./core/deploy/components-vagrant/k8s-cluster/scripts/cfg_worker_docker_registry.sh mycloud 1 192.168.68.160
./core/deploy/components-vagrant/k8s-cluster/scripts/create_mycloud_mount_folders.sh mycloud 1

kubectl apply -f ./core/deploy/components-k8s/storageClass.yml
kubectl apply -f ./core/deploy/components-k8s/deployPostgres/secret.yml
kubectl apply -f ./core/deploy/components-k8s/deployPostgres/deployment.yml

./core/deploy/components-k8s/deployMyCloud/scripts/setup_configmap.sh
./src/mycloud-api/docker_build_deploy.sh 1.0

cd "$_PWD"