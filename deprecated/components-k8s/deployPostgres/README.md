# Generate postgres password token

``` sh
echo -n "postgresadmin" | base64 # cG9zdGdyZXNhZG1pbg==
```
